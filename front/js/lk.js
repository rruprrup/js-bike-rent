let lkBikeCard;
let order;
let returnButtons = document.querySelectorAll("[data-order-id]");
returnButtons.forEach(button => {
  button.addEventListener("click", () => {
    order = button.dataset.orderId;
    deleteBike(order);
    lkBikeCard = button.parentElement.parentElement;
  });
});

async function deleteBike(order) {
  let options = {
    method: "DELETE",
    credentials: "include"
  };
  let response = await fetch(`/api/order/${order}`, options);
  if (response.status === 200) {
    lkBikeCard.remove();
  } else {
    throw new Error(`${response.status} ${response.statusText}`);
  }

}

// <div>
//     Нет арендованных велосипедов
//   </div>