let submitButton = document.querySelector("#reg-submit");
submitButton.addEventListener("click", (event => {
  event.preventDefault();
  cardNumberValidation();
  cardDateValidation();
  cardCvvValidation()
  if (cardNumberIsValid && cardDateIsValid && cardCvvIsValid) {
    submitAction();
  }
}));

async function submitAction() {
  let options = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json; charset=UTF-8"
    },
    credentials: "include",
    body: JSON.stringify({
      number: cardNumberInput.value,
      date: cardDateInput.value,
      cvv: cardCvvInput.value
    })
  };
  let response = await fetch("/api/card-requisites", options);
  let orderUrl = new URL(window.location.href).searchParams.get('back_url');

  if (response.status === 200) {
    window.location.href = (orderUrl === null) ? "/lk" : orderUrl;
  } else {
    throw new Error(`${response.status} ${response.statusText}`);
  }
}



function isNumber(expression) {
  return !isNaN(expression);
}


let cardNumberIsValid = false;
let cardDateIsValid = false;
let cardCvvIsValid = false;


let cardNumberInput = document.getElementById("card-number");
cardNumberInput.addEventListener("blur", cardNumberValidation);
let cardNumberP = document.getElementById("cardNumberP");

function cardNumberValidation() {
  cardNumberIsValid = false;
  if (cardNumberInput.value === "") {
    return;
  } else {
    let numberValue = cardNumberInput.value;
    let numberValueNoSpace = numberValue.split(" ").join("");
    if (!isNumber(numberValueNoSpace)) {
      renderError("Введите 16 цифр своего номера карты", cardNumberP);
    } else if (numberValueNoSpace.length < 1) {
      renderError("Поле не может быть пустым", cardNumberP);
    } else if (numberValueNoSpace.length > 0 && numberValueNoSpace.length !== 16) {
      renderError("Введите 16 цифр своего номера карты", cardNumberP);
    } else {
      hideError(cardNumberP);
      cardNumberIsValid = true;
      cardNumberInput.value = `${numberValueNoSpace.substr(0, 4)} ${numberValueNoSpace.substr(4, 4)} ${numberValueNoSpace.substr(8, 4)} ${numberValueNoSpace.substr(12, 4)}`;
    }
  }
}


let cardDateInput = document.getElementById("card-date");
cardDateInput.addEventListener("blur", cardDateValidation);
let cardDateP = document.getElementById("cardDateP");
let currentDate = new Date();
let currentYear = currentDate.getFullYear().toString().substr(2, 2);
let currentMonth = currentDate.getMonth() + 1;

function cardDateValidation() {
  cardDateIsValid = false;
  if (cardDateInput.value === "") {
    return;
  } else {
    let dateValue = cardDateInput.value;
    let dateValueNumbers = dateValue.split("").filter(x => isNumber(x) && x !== " ").join("");
    let dateValueFormated = `${dateValueNumbers.substr(0, 2)}/${dateValueNumbers.substr(2, 2)}`;
    cardDateInput.value = dateValueFormated;
    dateValue = dateValueFormated;
    if (Number(dateValue.substr(0, 2)) > 12 || Number(dateValue.substr(0, 2)) < 1) {
      renderError("Введите дату в формате мм/гг", cardDateP);
    }
    else if (Number(dateValue.substr(3, 2)) < currentYear || Number(dateValue.substr(3, 2)) > (Number(currentYear) + 5)) {
      renderError("Введите дату в формате мм/гг", cardDateP);
    }
    else if (dateValue.substr(3, 2) === currentYear && dateValue.substr(0, 2) <= currentMonth) {
      renderError("Срок действия вашей карты истёк. Введите данные другой карты.", cardDateP);
    }
    else if (dateValue.length !== 5) {
      renderError("Введите дату в формате мм/гг", cardDateP);
    }
    else {
      hideError(cardDateP);
      cardDateIsValid = true;
    }
  }
}


let cardCvvInput = document.getElementById("card-cvv");
cardCvvInput.addEventListener("blur", cardCvvValidation);
let cardCvvP = document.getElementById("cardCvvP");

function cardCvvValidation() {
  cardCvvIsValid = false;
  if (cardCvvInput.value === "") {
    return;
  } else {
    let cvvValue = cardCvvInput.value;
    if (!isNumber(cvvValue) || cvvValue.length !== 3) {
      renderError("Введите трёхзначный код с оборота карты", cardCvvP);
    } else {
      hideError(cardCvvP);
      cardCvvIsValid = true;
    }
  }
}


function renderError(error, elem) {
  elem.textContent = error;
  elem.classList.remove("hide");
}


function hideError(elem) {
  elem.classList.add("hide");
}
