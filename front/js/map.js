ymaps.ready(init);

function init() {
  let myMap = new ymaps.Map(document.querySelector("#map"), {
    center: [55.03, 82.91],
    zoom: 11
  });

  function pointerInfo() {
    api.getPointers().then((result) => {

      result.forEach((element) => {
        let pointerCoordinates = element.coordinates;
        let pointerAddress = element.address;
        let pointer_id = element._id;
        let myPlacemark = new ymaps.Placemark(
          pointerCoordinates,
          {
            balloonContent: `<p><a href="/catalog/${pointer_id}">${pointerAddress}<a></p>`,
          },
          {
            preset: 'islands#redIcon'
          }
        );
        myMap.geoObjects.add(myPlacemark);
      });
    });
  }
  pointerInfo();
}