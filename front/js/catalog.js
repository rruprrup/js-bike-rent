'use strict';

const loadMore = document.querySelector("#loadMore button");
const bikeList = document.querySelector("#bikeList");

function init() {
  let pageCounter = 1;
  loadCatalog(pageCounter);
  loadMore.addEventListener("click", () => {
    disableButtonLoadMore();
    pageCounter++;
    loadCatalog(pageCounter)
  });
}

function loadCatalog(page) {
  api.getBikes(getPointId(), page).then((result) => {
    enableButtonLoadMore();
    let items = result.bikesList;
    let hasMore = result.hasMore;
    appendCatalog(items);
    showButtonLoadMore(hasMore);
  });
}

function appendCatalog(items) {
  const itemList = bikeList.querySelector("ul");
  items.forEach(item => {
    const li = document.createElement("li");
    itemList.appendChild(li);
    li.innerHTML += `<div class="item-border"><img src="/images/${item.img}"></div>
                     <h2>${item.name}</h2>                
                     <p>Стоимость за час ${item.cost * 60}₽</p>
                     <button><a href="/order/${item._id}">Арендовать</a></button>`;
  });
}

function showButtonLoadMore(hasMore) {
  if (hasMore) {
    document.querySelector("#loadMore").classList.remove("hidden");
  } else {
    document.querySelector("#loadMore").classList.add("hidden");
  }
}

function disableButtonLoadMore() {
  loadMore.setAttribute("disabled", true);
}

function enableButtonLoadMore() {
  loadMore.removeAttribute("disabled");
}

function getPointId() {
  let arr = document.URL.split("catalog/");
  return arr[1];
}

document.addEventListener('DOMContentLoaded', init);
